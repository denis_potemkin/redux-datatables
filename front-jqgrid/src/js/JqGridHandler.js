import 'jqGrid/js/jquery.jqGrid.min';
import 'jqGrid/js/grid.filter';
import 'jqGrid/plugins/grid.addons';
import 'jqGrid/js/i18n/grid.locale-en';
import 'jqGrid/css/ui.jqgrid-bootstrap.css';
import 'jqGrid/css/ui.jqgrid-bootstrap-ui.css';
import { JqGridFilterHandler } from "./JqGridFilterHandler";

import { EMPLOYEE_BASE_URL } from './constants';
import { Filter } from "./Filter";

export class JqGridHandler {

  constructor(domId) {
    this.tableId = domId;
    this.tableElement = $('#' + domId);
    this.initTable().then(data => this._table = data);
    this.filters = [];
  }

  get table() {
    return this._table;
  }


  get filters() {
    return this._filters;
  }

  set filters(value) {
    this._filters = value;
  }

  async initTable() {
    return this.tableElement.jqGrid({
      datatype: 'json',
      jsonReader: {
        root: data => data._embedded.employees,
        page: data => data.page.number,
        total: data => data.page.totalPages,
        records: data => data.page.totalElements,
        rows: data => data.page.size
      },
      colNames: [
        '',
        '',
        '',
        ''
      ],
      colModel: [
        {
          name: 'name',
          index: 'name',
          width: 300,
          sortable: false,
        },
        {
          name: 'email',
          index: 'email',
          width: 300,
          sortable: false,
        },
        {
          name: 'phone',
          index: 'phone',
          width: 300,
          sortable: false,
        },
        {
          name: 'favoriteMovie',
          index: 'favoriteMovie',
          width: 300,
          sortable: false,
        }
      ],
      pager: '#employeePager',
      pgbuttons: true,

      height: 'auto',
      width: 'auto',

      styleUI: 'Bootstrap',

      url: await this.getURL(),

      loadonce: false,

      viewrecords: true,

      mtype: 'GET',
      ajaxGridOptions: {
        contentType: 'application/json; charset=utf-8'
      },

      serializeGridData: function (postData) {
        postData.page--;
        return postData;
      },

      loadComplete: () => {
        let colModelNames = this.tableElement
            .jqGrid()
            .getGridParam('colModel')
            .map(column => column.name);

        this.addFilters('', colModelNames);

        const filterInputs = Object.values($('.filter'));

        this.dbFilters.forEach(filter => {
          filterInputs.find(input => input.id === filter.name).value = filter.value;
        });
      }
    });
  }

  addFilters(prefix, colModelNames) {
    const that = this;

    $(document).find('.ui-th-column').each(function (index) {
      let colModelName = colModelNames[index];
      if (colModelName.upperCase().split(' ').length > 1) {
        colModelName = colModelName
            .upperCase()
            .split(' ')
            .map(word => word.capitalize())
            .join(' ');
      } else {
        colModelName = colModelName.capitalize();
      }

      let title = prefix ? prefix + ' ' + colModelName : colModelName;
      let id = colModelNames[index];

      $(this).html('<input class="filter" type="text" placeholder="' + title + '" id="' + id + '"/>');
      if (!that.filters.isEmpty()) {
        let filter = that.filters.find(filter => filter.name === id) || {};
        $(this).find('.filter').val(filter.value);
      }
    });

    $(document).find('.ui-th-column').each(function () {
      $('input').on('keyup', function () {
        that.filters = that.filters.cloneAndPut(new Filter($('#employees').attr('id'), this.id, this.value));
        that.navigate(this.id, this.value);
      });
    });
  }

  navigate(key, value) {
    let url = this.tableElement.jqGrid().getGridParam('url');
    return this.tableElement
        .jqGrid()
        .setGridParam({url: this.calculateUrl(url, key, value)})
        .trigger('reloadGrid');
  }

  calculateUrl(oldUrl, key, value) {
    let newKey = key;
    const newFilter = newKey + '=' + value;
    if (oldUrl.indexOf('&' + newKey) > -1) {
      return oldUrl.split('&')
          .map(item => !item.split('=')[1] ? '' : item.includes(newKey + '=') ? newFilter : item)
          .join('&');
    }
    return oldUrl + '&' + newFilter;
  }

  async getURL() {
    this.dataTableFilterHandler = new JqGridFilterHandler();
    this.dbFilters = await this.dataTableFilterHandler.findFilters('employees') || [];
    let url = '';
    this.dbFilters.forEach((filter) => url = this.calculateUrl(EMPLOYEE_BASE_URL, filter.name, filter.value));
    return url || EMPLOYEE_BASE_URL;
  }
}