import './polyfills';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { EMPLOYEE_BASE_URL } from './constants';
import { JqGridHandler } from './JqGridHandler';
import { JqGridFilterHandler } from "./JqGridFilterHandler";
import { Filter } from "./Filter";

let jqGridHandler;

const TABLE_ELEMENT = $('#employees');

$(function () {
  jqGridHandler = new JqGridHandler('employees');

  let jqGridFilterHandler = new JqGridFilterHandler();

  $('#save').on('click', () => {
    jqGridFilterHandler.saveFilters(...jqGridHandler.filters)
  });
  $('#clear').on('click', () => {
    jqGridFilterHandler.clearFilters('employees');
    $(document).find('.ui-th-column').find('.filter').val('');
    sessionStorage.removeItem('filters');
    jqGridHandler.tableElement
        .jqGrid()
        .setGridParam({ url: EMPLOYEE_BASE_URL })
        .trigger('reloadGrid');
  });
});
