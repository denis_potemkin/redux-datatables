export const API_URL = 'http://localhost:9080/api/';

export const EMPLOYEE_BASE_URL = API_URL + 'employees/search/findEmployees?projection=employeePluginDemo';

export const FILTERS_BASE_URL = API_URL + 'filters';

export const FETCH_HEADERS = new Headers({
  'Content-Type': 'application/json; charset=utf-8',
});
