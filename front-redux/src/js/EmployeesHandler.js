import { ACTION_TYPES, EMPLOYEE_BASE_URL, FETCH_HEADERS, FIND_MOVIE_BY_TITLE_URL } from "./constants";

const store = window.store;

export class EmployeesHandler {

  constructor() {
  }

  fetchFavoriteMovie(title) {
    return dispatch => fetch(FIND_MOVIE_BY_TITLE_URL + title)
        .then(response => response.json())
        .then(movie => dispatch({ type: ACTION_TYPES.FETCH_FAVORITE_MOVIE, payload: movie._links.self.href }));
  }

  saveEmployee(employee) {
    window.store.dispatch(this.fetchFavoriteMovie(employee.favoriteMovie)).then(() => {
      employee.favoriteMovie = window.store.getState().employee.favoriteMovie;
      window.store.dispatch(dispatch => fetch(employee._links.self.href, {
        body: JSON.stringify(employee),
        method: 'PATCH',
        headers: FETCH_HEADERS,
      }).then()
          .then(() => {
            alert('Saved');
            this.renderEmployees();
          }).catch(error => alert('Something went wrong ' + error)));
    })
  }

  fetchEmployees() {
    return dispatch => fetch(EMPLOYEE_BASE_URL)
        .then(response => response.json())
        .then(employees => dispatch({ type: ACTION_TYPES.FETCH_EMPLOYEES_TABLE_DATA, payload: employees._embedded.employees }));
  }

  fetchEmployee(url) {
    return dispatch => fetch(url)
        .then(response => response.json())
        .then(employee => dispatch({ type: ACTION_TYPES.FETCH_EMPLOYEE, payload: employee }));
  }

  renderEmployees() {
    window.store.dispatch(this.fetchEmployees()).then(() => {});
  }

  onSaveClick(event) {
    event.preventDefault();

    const updatedEmployee = {
      name: $('#name').val(),
      email: $('#email').val(),
      phone: $('#phone').val(),
      favoriteMovie: $('#favoriteMovie').val(),
      _links: window.store.getState().employee.renderEmployee._links
    };

    window.store.dispatch(dispatch => {
      dispatch({ type: ACTION_TYPES.SAVE_EMPLOYEE, payload: updatedEmployee });
      employeesHandler.saveEmployee(updatedEmployee);
    });
  }

  mapEmployeeToTr(employee) {
    const tr = document.createElement('tr');
    tr.innerHTML = `<tr>
                    <td>${employee.name}</td>
                    <td>${employee.email}</td>
                    <td>${employee.phone}</td>
                    <td>${employee.favoriteMovie}</td>
                 </tr>`;

    tr.onclick = function () {
      renderEmployeeToDiv(employee._links.self.href);
    };
    return tr;
  }

  mapEmployeeUrl(url) {
    return url += '?projection=employeePluginDemo';
  }
}

const employeesHandler = new EmployeesHandler();

function renderEmployeeToDiv(url) {
  $('#employee-card').prop('hidden', '');

  $('#employee').html('');

  window.store.dispatch(employeesHandler.fetchEmployee(employeesHandler.mapEmployeeUrl(url))).then(() => {
    const div = document.createElement('div');
    div.setAttribute('class', 'form-group');
    div.setAttribute('data-url', url);
    div.innerHTML = `<label for="name">Name</label>
                   <input class="form-control" id="name" value="${window.store.getState().employee.renderEmployee.name}">
                   <label for="email">Email</label>
                   <input class="form-control" id="email" value="${window.store.getState().employee.renderEmployee.email}">
                   <label for="phone">Phone</label>
                   <input class="form-control" id="phone" value="${window.store.getState().employee.renderEmployee.phone}">
                   <label for="favoriteMovie">Favorite Movie</label>
                   <input class="form-control" id="favoriteMovie" value="${window.store.getState().employee.renderEmployee.favoriteMovie}">`;
    document.querySelector('#employee').appendChild(div);
  });
}

