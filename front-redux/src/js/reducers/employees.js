import { ACTION_TYPES } from "../constants";

export default function employees(state = [], action) {
  switch (action.type) {
    case ACTION_TYPES.FETCH_EMPLOYEES_TABLE_DATA: {
      return action.payload;
    }
    default: {
      return state;
    }
  }
}