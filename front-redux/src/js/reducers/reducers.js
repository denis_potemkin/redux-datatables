import { combineReducers } from 'redux';

import employees from "./employees";
import employee from "./employee";

export default combineReducers({
  employees,
  employee
})