import { ACTION_TYPES } from "../constants";

export default function employee(state = {}, action) {
  switch (action.type) {
    case ACTION_TYPES.FETCH_EMPLOYEE: {
      return Object.assign({}, state, {
        renderEmployee: action.payload
      });
    }
    case ACTION_TYPES.SAVE_EMPLOYEE: {
      return Object.assign({}, state, {
        saveEmployee: action.payload
      });
    }
    case ACTION_TYPES.FETCH_FAVORITE_MOVIE: {
      return Object.assign({}, state, {
        favoriteMovie: action.payload
      });
    }
    default: {
      return state;
    }
  }
}