export const API_URL = 'http://localhost:9080/api/';

export const EMPLOYEE_BASE_URL = API_URL + 'employees/search/findEmployees?projection=employeePluginDemo';

export const FIND_MOVIE_BY_TITLE_URL = API_URL + 'movies/search/findFirstByTitle?title=';

export const FILTERS_BASE_URL = API_URL + 'filters';

export const FETCH_HEADERS = new Headers({
  'Content-Type': 'application/json; charset=utf-8',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Request-Method': 'PATCH'
});

export const ACTION_TYPES = {
  ADD_FILTER: 'ADD_FILTER',
  CLEAR_FILTERS: 'CLEAR_FILTERS',

  FIND_DB_FILTERS: 'FIND_DB_FILTERS',
  CLEAR_DB_FILTERS: 'CLEAR_DB_FILTERS',

  FETCH_EMPLOYEES_TABLE_DATA: 'FETCH_EMPLOYEES_TABLE_DATA',

  FETCH_FAVORITE_MOVIE: 'FETCH_FAVORITE_MOVIE',
  FETCH_EMPLOYEE: 'FETCH_EMPLOYEE',
  SAVE_EMPLOYEE: 'SAVE_EMPLOYEE',

  TABLE_DATA: 'TABLE_DATA',
  INIT_TABLE: 'INIT_TABLE'
};
