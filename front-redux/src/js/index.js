import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import reducers from "./reducers/reducers";
import { EmployeesHandler } from "./EmployeesHandler";

const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk)));
window.store = store;

$(function () {
  const table = document.querySelector('#employees');
  const tbody = document.createElement('tbody');
  table.appendChild(tbody);

  store.subscribe(() => {
    tbody.innerHTML = '';
    store.getState().employees.map(employeesHandler.mapEmployeeToTr).forEach(employee => {
      tbody.appendChild(employee);
    });
  });

  const employeesHandler = new EmployeesHandler();
  employeesHandler.renderEmployees();

  $('#save').on('click', employeesHandler.onSaveClick);
});