package ua.com.cs.demo.configuration;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;
import ua.com.cs.demo.persistence.model.Filter;
import ua.com.cs.demo.persistence.repository.FilterRepository;

import java.util.Objects;

@RepositoryEventHandler
@FieldDefaults(level = AccessLevel.PRIVATE)
@Component
@AllArgsConstructor
public class FilterEventHandler {

    FilterRepository filterRepository;

    @HandleBeforeCreate
    public void handleEntityCreate(Filter filter) {
        populateFilterId(filter);
        filterRepository.save(filter);
    }

    private void populateFilterId(Filter filter) {
        Filter oldFilter = filterRepository.findByTableNameAndNameIgnoreCase(filter.getTableName(), filter.getName());
        if (Objects.nonNull(oldFilter)) {
            filter.setId(oldFilter.getId());
        }
    }
}
