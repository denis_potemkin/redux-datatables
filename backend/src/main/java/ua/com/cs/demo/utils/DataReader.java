package ua.com.cs.demo.utils;

import com.opencsv.bean.CsvToBeanBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class DataReader {

    public List<String> readResourceData(String resourceName) {
        try {
            return Files.readAllLines(Paths.get(extractPath(resourceName)), Charset.defaultCharset());
        } catch (IOException e) {
            return Collections.emptyList();
        }
    }

    public <T> List<T> readCsvResourceData(String resourceName, Class<T> expectedClass) {
        return new CsvToBeanBuilder<T>(
            new StringReader(
                readResourceData(resourceName)
                    .stream()
                    .collect(Collectors.joining("\n"))
            )).withType(expectedClass)
            .withSeparator('|')
            .build()
            .parse();
    }

    private String extractPath(String resourceName) {
        String path = Optional.ofNullable(
            this.getClass()
                .getClassLoader()
                .getResource(resourceName))
            .orElseThrow(IllegalArgumentException::new)
            .getFile();

        if (StringUtils.startsWithAny(path, "\\", "/")) {
            path = path.substring(1);
        }
        return path;
    }
}
