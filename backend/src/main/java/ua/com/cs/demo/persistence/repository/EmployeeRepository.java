package ua.com.cs.demo.persistence.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import ua.com.cs.demo.persistence.model.Employee;
import ua.com.cs.demo.persistence.projection.EmployeePluginDemoProjection;


@CrossOrigin
@RepositoryRestResource(excerptProjection = EmployeePluginDemoProjection.class)
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

    @Query("select e from Employee e where " +
        "(:name is null or upper(e.name) like upper(concat('%', :name, '%'))) and" +
        "(:email is null or upper(e.email) like upper(concat('%', :email, '%'))) and" +
        "(:phone is null or e.phone like concat('%',substring(:phone, 2),'%')) and" +
        "(:favoriteMovie is null or e.favoriteMovie in " +
        "   (select m from Movie m where upper(m.title) like upper(concat('%', :favoriteMovie, '%'))))")
    Page<Employee> findEmployees(@Param("name") String name,
                                 @Param("email") String email,
                                 @Param("phone") String phone,
                                 @Param("favoriteMovie") String favoriteMovie,
                                 Pageable pageable);

    Employee findByEmail(String email);
}