package ua.com.cs.demo.persistence.model;

import com.opencsv.bean.CsvBindByName;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
@Table(name = "movie")
@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @CsvBindByName
    String title;

    @CsvBindByName
    String director;

    @CsvBindByName
    Integer year;

    @CsvBindByName
    String country;

    @CsvBindByName
    Integer duration;

}
