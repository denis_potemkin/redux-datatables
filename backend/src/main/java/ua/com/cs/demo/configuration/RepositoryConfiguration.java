package ua.com.cs.demo.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import ua.com.cs.demo.persistence.projection.EmployeeDetailedProjection;
import ua.com.cs.demo.persistence.projection.EmployeePluginDemoProjection;
import ua.com.cs.demo.persistence.projection.EmployeeWithoutPhotoProjection;

@Configuration
public class RepositoryConfiguration extends RepositoryRestConfigurerAdapter {

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration configuration) {
        configuration.getProjectionConfiguration().addProjection(EmployeeDetailedProjection.class);
        configuration.getProjectionConfiguration().addProjection(EmployeeWithoutPhotoProjection.class);
        configuration.getProjectionConfiguration().addProjection(EmployeePluginDemoProjection.class);
    }
}
