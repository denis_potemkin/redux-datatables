package ua.com.cs.demo.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import ua.com.cs.demo.persistence.model.Position;
import ua.com.cs.demo.persistence.projection.PositionProjection;

@CrossOrigin
@RepositoryRestResource(excerptProjection = PositionProjection.class)
public interface PositionRepository extends CrudRepository<Position, Long> {

    Position findByName(@Param("name") String name);
}
