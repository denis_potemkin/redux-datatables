package ua.com.cs.demo.persistence.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import ua.com.cs.demo.persistence.model.Filter;
import ua.com.cs.demo.persistence.projection.FilterProjection;

import java.util.List;

@CrossOrigin
@RepositoryRestResource(excerptProjection = FilterProjection.class)
public interface FilterRepository extends CrudRepository<Filter, Long>, JpaSpecificationExecutor<Filter> {

    List<Filter> findByTableNameIgnoreCase(@Param("tableName") String name);

    Filter findByTableNameAndNameIgnoreCase(String tableName, String name);

}
