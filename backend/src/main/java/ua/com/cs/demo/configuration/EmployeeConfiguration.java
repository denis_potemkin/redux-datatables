package ua.com.cs.demo.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Data
@Component
@PropertySource("classpath:employee.yml")
@Validated
@ConfigurationProperties
public class EmployeeConfiguration {

    @Min(0)
    int employeeAmount;
}
