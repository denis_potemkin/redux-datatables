package ua.com.cs.demo.persistence.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
@Table(name = "filter", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"tableName", "name"})
})
@Entity
public class Filter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    String tableName;

    String name;

    String value;
}
