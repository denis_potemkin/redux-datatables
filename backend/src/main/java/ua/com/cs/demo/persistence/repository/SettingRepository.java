package ua.com.cs.demo.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import ua.com.cs.demo.persistence.model.Setting;

import java.util.Collection;
import java.util.List;

@CrossOrigin
public interface SettingRepository extends CrudRepository<Setting, Long> {

    Setting findByName(@Param("name") String name);

    List<Setting> findByNameIn(@Param("names") Collection<String> names);
}
