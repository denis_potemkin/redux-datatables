package ua.com.cs.demo.persistence.projection;

import org.springframework.data.rest.core.config.Projection;
import ua.com.cs.demo.persistence.model.Position;

@Projection(name = "projection", types = {Position.class})
public interface PositionProjection {

    String getName();
}
