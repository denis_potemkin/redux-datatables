package ua.com.cs.demo.controller;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.com.cs.demo.persistence.model.Filter;
import ua.com.cs.demo.persistence.repository.FilterRepository;

import java.util.Collections;
import java.util.List;

@CrossOrigin
@RepositoryRestController
@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
public class FilterController {

    @NonNull
    FilterRepository filterRepository;

    @RequestMapping(value = "filters/clearFilters", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
    public @ResponseBody
    ResponseEntity<List<Filter>> clearFilters(@RequestBody String tableName) {
        List<Filter> filters = filterRepository.findByTableNameIgnoreCase(tableName);

        filterRepository.delete(filters);

        return ResponseEntity.ok(Collections.emptyList());
    }
}
