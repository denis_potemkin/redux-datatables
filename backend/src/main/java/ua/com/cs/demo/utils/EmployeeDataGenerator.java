package ua.com.cs.demo.utils;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ua.com.cs.demo.persistence.model.Movie;
import ua.com.cs.demo.persistence.model.Position;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static java.util.stream.Collectors.toList;

@Component
@RequiredArgsConstructor
public class EmployeeDataGenerator {

    private static Random random = new Random();

    private static List<String> maleFirstNames;

    private static List<String> femaleFirstNames;

    private static List<String> lastNames;

    @NonNull
    private DataReader dataReader;

    public Date getRandomBirthDate() {
        Random random = new Random();
        int randomYear = 1950 + random.nextInt(66);
        int randomMonth = 1 + random.nextInt(11);
        int randomDay = randomMonth == 2 ? 1 + random.nextInt(27) : 1 + random.nextInt(30);
        return Date.valueOf(LocalDate.of(randomYear, randomMonth, randomDay));
    }

    public String getRandomMaleFirstName() {
        maleFirstNames = Objects.isNull(maleFirstNames) ? dataReader.readResourceData(PublicConstants.MALE_FIRST_NAMES_RESOURCE) : maleFirstNames;
        return StringUtils.capitalize(maleFirstNames.get(random.nextInt(maleFirstNames.size() - 1)).toLowerCase());
    }

    public String getRandomFemaleFirstName() {
        femaleFirstNames = Objects.isNull(femaleFirstNames) ? dataReader.readResourceData(PublicConstants.FEMALE_FIRST_NAMES_RESOURCE) : femaleFirstNames;
        return StringUtils.capitalize(femaleFirstNames.get(random.nextInt(femaleFirstNames.size() - 1)).toLowerCase());
    }

    public String getRandomLastName() {
        lastNames = Objects.isNull(lastNames) ? dataReader.readResourceData(PublicConstants.LAST_NAMES_RESOURCE) : lastNames;
        return StringUtils.capitalize(lastNames.get(random.nextInt(lastNames.size() - 1)).toLowerCase());
    }

    public String getRandomFirstName() {
        return random.nextBoolean() ? getRandomMaleFirstName() : getRandomFemaleFirstName();
    }

    public String getRandomName() {
        return getRandomFirstName() + " " + getRandomLastName();
    }

    public String getRandomPhone() {
        return "+380" + (100_000_000 + random.nextInt(899_999_999));
    }

    public Position getRandomPosition(List<Position> positions) {
        return positions.get(random.nextInt(positions.size()));
    }

    public Movie getRandomMovie(List<Movie> movies) {
        return movies.get(random.nextInt(movies.size()));
    }

    public List<Position> getPositionsFromFS() {
        List<String> positionNames = dataReader.readResourceData(PublicConstants.POSITION_NAMES_RESOURCE);

        return positionNames.stream().map(new Position()::setName).collect(toList());
    }

    public List<Movie> getMoviesFromFS() {
        try {
            return dataReader.readCsvResourceData(PublicConstants.MOVIES_RESOURCE, Movie.class);
        } catch (Exception e) {
            System.err.println(e.toString());
            return Collections.emptyList();
        }
    }
}
