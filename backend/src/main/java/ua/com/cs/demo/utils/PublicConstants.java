package ua.com.cs.demo.utils;

public abstract class PublicConstants {

    public static final String PHONE_PATTERN_REGEX = "^(1\\s|1|)?([+]\\d{2})((\\(\\d{3}\\))|\\d{3})(\\-|\\s)?(\\d{3})(\\-|\\s)?(\\d{4})$";

    public static final String MALE_FIRST_NAMES_RESOURCE = "data/male-first-names.dat";

    public static final String FEMALE_FIRST_NAMES_RESOURCE = "data/female-first-names.dat";

    public static final String LAST_NAMES_RESOURCE = "data/last-names.dat";

    public static final String POSITION_NAMES_RESOURCE = "data/position-names.dat";

    public static final String MOVIES_RESOURCE = "data/movies.csv";
}
