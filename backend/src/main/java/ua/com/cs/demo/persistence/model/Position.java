package ua.com.cs.demo.persistence.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
@Table(name = "position", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name"})
})
@Entity
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Length(min = 2, max = 30)
    @Valid
    String name;
}
