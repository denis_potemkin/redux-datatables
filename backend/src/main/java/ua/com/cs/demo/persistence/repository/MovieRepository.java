package ua.com.cs.demo.persistence.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import ua.com.cs.demo.persistence.model.Movie;

@CrossOrigin
@RepositoryRestResource
public interface MovieRepository extends CrudRepository<Movie, Long> {

    Movie findFirstByTitle(@Param("title") String title);
}
