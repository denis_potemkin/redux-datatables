package ua.com.cs.demo.utils.initialization;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ua.com.cs.demo.configuration.EmployeeConfiguration;
import ua.com.cs.demo.persistence.model.Employee;
import ua.com.cs.demo.persistence.model.Movie;
import ua.com.cs.demo.persistence.model.Position;
import ua.com.cs.demo.persistence.model.Setting;
import ua.com.cs.demo.persistence.repository.EmployeeRepository;
import ua.com.cs.demo.persistence.repository.MovieRepository;
import ua.com.cs.demo.persistence.repository.PositionRepository;
import ua.com.cs.demo.persistence.repository.SettingRepository;
import ua.com.cs.demo.utils.EmployeeDataGenerator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

@FieldDefaults(level = AccessLevel.PRIVATE)
@RequiredArgsConstructor
@Component
public class DatabaseInitializer implements CommandLineRunner {

    @NonNull
    EmployeeDataGenerator employeeDataGenerator;

    @NonNull
    EmployeeRepository employeeRepository;

    @NonNull
    PositionRepository positionRepository;

    @NonNull
    SettingRepository settingRepository;

    @NonNull
    MovieRepository movieRepository;

    @NonNull
    EmployeeConfiguration employeeConfiguration;

    List<Position> positions;

    List<Movie> movies;

    @Override
    public void run(String... strings) {
        positions = generatePositions();
        movies = generateMovies();

        IntStream.range(0, employeeConfiguration.getEmployeeAmount()).parallel().forEach(this::generateRandomEmployee);
        createSettings();
    }

    private void generateRandomEmployee(int index) {
        Employee employee = new Employee();

        employee.setBirthday(employeeDataGenerator.getRandomBirthDate())
            .setPosition(employeeDataGenerator.getRandomPosition(positions))
            .setFavoriteMovie(employeeDataGenerator.getRandomMovie(movies))
            .setName(employeeDataGenerator.getRandomName())
            .setEmail(employee.getName().replace(" ", "") + index + "@gmail.com")
            .setPhone(employeeDataGenerator.getRandomPhone());

        employeeRepository.save(employee);
    }

    private List<Position> generatePositions() {
        return (List<Position>) positionRepository.save(employeeDataGenerator.getPositionsFromFS());
    }

    private void createSettings() {
        Setting displayPhoto = new Setting()
            .setName("displayPhoto")
            .setValue("true")
            .setPossibleValues(Arrays.asList("true", "false"));
        Setting colorTheme = new Setting()
            .setName("colorTheme")
            .setValue("Blue")
            .setPossibleValues(Arrays.asList("Blue", "Red", "Green"));
        settingRepository.save(Arrays.asList(displayPhoto, colorTheme));
    }

    private List<Movie> generateMovies() {
        return (List<Movie>) movieRepository.save(employeeDataGenerator.getMoviesFromFS());
    }
}
