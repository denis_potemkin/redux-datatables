package ua.com.cs.demo.persistence.model;


import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import ua.com.cs.demo.utils.PublicConstants;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.sql.Date;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Accessors(chain = true)
@Table(name = "employee", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"email"})
})
@Entity
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @Length(min = 2, max = 80)
    @Valid
    String name;

    @NotBlank
    @Email
    @Valid
    String email;

    @Pattern(regexp = PublicConstants.PHONE_PATTERN_REGEX)
    @Valid
    String phone;

    @Past
    @Valid
    Date birthday;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "position_id")
    Position position;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "favorite_movie_id")
    Movie favoriteMovie;

    @Setter
    @Lob
    byte[] photo;
}