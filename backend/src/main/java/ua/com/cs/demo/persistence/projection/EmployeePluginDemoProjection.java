package ua.com.cs.demo.persistence.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import ua.com.cs.demo.persistence.model.Employee;

@Projection(name = "employeePluginDemo", types = {Employee.class})
public interface EmployeePluginDemoProjection {

    String getName();

    String getEmail();

    String getPhone();

    @Value("#{target.favoriteMovie.title}")
    String getFavoriteMovie();
}
