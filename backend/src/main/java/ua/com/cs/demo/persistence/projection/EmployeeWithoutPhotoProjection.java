package ua.com.cs.demo.persistence.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import ua.com.cs.demo.persistence.model.Employee;

import java.sql.Date;

@Projection(name="employeeWithoutPhoto", types={Employee.class})
public interface EmployeeWithoutPhotoProjection {

    String getName();

    String getEmail();

    String getPhone();

    Date getBirthday();

    @Value("#{target.position.name}")
    String getPosition();
}
