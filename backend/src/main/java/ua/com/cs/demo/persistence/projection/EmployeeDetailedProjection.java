package ua.com.cs.demo.persistence.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import ua.com.cs.demo.persistence.model.Employee;

import java.sql.Date;

@Projection(name = "employeeDetailed", types = {Employee.class})
public interface EmployeeDetailedProjection {

    String getName();

    String getEmail();

    String getPhone();

    Date getBirthday();

    @Value("#{target.position.name}")
    String getPosition();

    @Value("#{target.favoriteMovie.title}")
    String getFavoriteMovie();

    byte[] getPhoto();
}
