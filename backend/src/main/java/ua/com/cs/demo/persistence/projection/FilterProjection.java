package ua.com.cs.demo.persistence.projection;

import org.springframework.data.rest.core.config.Projection;
import ua.com.cs.demo.persistence.model.Filter;

@Projection(name = "projection", types = {Filter.class})
public interface FilterProjection {

    String getTableName();

    String getName();

    String getValue();
}
