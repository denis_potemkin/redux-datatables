import { FETCH_HEADERS, FILTERS_BASE_URL } from './constants';

export class DataTableFilterHandler {

  saveFilters(...filters) {
    Promise.all(filters.map(this.saveFilter))
        .then(() => alert('Saved'))
        .catch(error => alert('Something went wrong ' + error))
  }

  saveFilter(filter) {
    return fetch(FILTERS_BASE_URL, {
      body: JSON.stringify(filter),
      method: 'POST',
      headers: FETCH_HEADERS
    })
  }

  findFilters(tableName) {
    return fetch(FILTERS_BASE_URL + '/search/findByTableNameIgnoreCase?tableName=' + tableName)
        .then(response => response.json()).then(data => data._embedded.filters)
        .catch(error => alert('Something went wrong ' + error));
  }

  clearFilters(tableName) {
    return fetch(FILTERS_BASE_URL + '/clearFilters', {
      method: 'POST',
      body: JSON.stringify(tableName),
      headers: FETCH_HEADERS
    })
        .then(() => alert('Cleared'))
        .catch(error => alert('Something went wrong ' + error));
  }
}