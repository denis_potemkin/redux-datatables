Object.defineProperty(Object.prototype, 'equals', {
  value: function (other) {
    return _.isEqual(this, other);
  },
  enumerable: false
});

Array.prototype.isEmpty = function () {
  return this.length === 0;
};

Array.prototype.cloneAndPut = function (...data) {
  let newArray = [...this];

  data.forEach(element => {
    let index = newArray.indexOf(newArray.find(old => old.equals(element)));
    if (index > -1) {
      newArray.splice(index, 1);
    }
    newArray.push(element);
  });

  return newArray;
};

Array.prototype.deepCloneAndPut = function (...data) {

};

Array.prototype.distinct = function () {
  return _.uniqWith(this, (current, next) => current.equals(next));
};