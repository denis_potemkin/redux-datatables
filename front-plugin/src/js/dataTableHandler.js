import 'datatables.net';
import 'datatables.net-bs4';
import 'datatables.net-bs4/css/dataTables.bootstrap4.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { Filter } from './filter';
import { DataTableFilterHandler } from "./dataTableFilterHandler";

import { EMPLOYEE_BASE_URL } from './constants';

export class DataTableHandler {

  constructor(domId) {
    this.tableId = domId;
    this.tableElement = $('#' + domId);
    this.initTable().then(data => this._table = data);
    this.setupPagination();
    this._filters = [];
  }

  get table() {
    return this._table;
  }


  get filters() {
    return this._filters;
  }

  set filters(value) {
    this._filters = value;
  }

  async initTable() {
    return this.tableElement.DataTable({
      processing: false,
      ordering: false,
      serverSide: true,
      paging: true,
      pagingType: 'numbers',
      pageLength: 10,
      lengthChange: false,

      searching: false,
      ajax: {
        type: 'GET',
        url: await this.getURL(),
        dataFilter: (data) => {
          let metaData = JSON.parse(data);

          metaData.recordsTotal = metaData.page.totalElements;
          metaData.recordsFiltered = metaData.page.totalElements;
          metaData.pageLength = metaData.page.size;
          metaData.data = metaData._embedded.employees;

          return JSON.stringify(metaData);
        },
      },

      initComplete: () => {
        this.addFilters();

        $('.filter').on('keyup', (event) => {
          this.filters = this.filters.cloneAndPut(new Filter($('#employees').attr('id'), event.target.id, event.target.value));
        });

        const filterInputs = Object.values($('.filter'));

        this.dbFilters.forEach(filter => {
          filterInputs.find(input => input.id === filter.name).value = filter.value;
        });
      },

      columns: [
        {data: 'name'},
        {data: 'email'},
        {data: 'phone'},
        {data: 'favoriteMovie'}
      ]
    });
  }

  setupPagination() {
    this.tableElement.on('page.dt', () => {
      let info = this._table.page.info();
      this.navigate('page', info.page);
    });
  }

  addFilters(prefix) {
    this.tableElement.find('thead th').each(function () {
      let title = prefix ? prefix + ' ' + $(this).text() : $(this).text();
      let id = $(this).attr('id');
      $(this).html('<input class="filter" type="text" placeholder="' + title + '" id="' + id + '"/>');
    });

    let that = this;
    this.table.columns().every(function () {
      $('input', this.header()).on('keyup', function () {
        that.navigate(this.id, this.value);
      });
    });
  }

  navigate(key, value) {
    return this.table.ajax.url(this.calculateUrl(this.table.ajax.url(), key, value)).draw(false);
  }

  calculateUrl(oldUrl, key, value) {
    let newKey = key;
    const newFilter = newKey + '=' + value;
    if (oldUrl.indexOf('&' + newKey) > -1) {
      return oldUrl.split('&')
          .map(item => !item.split('=')[1] ? '' : item.includes(newKey + '=') ? newFilter : item)
          .join('&');
    }
    return oldUrl + '&' + newFilter;
  }

  async getURL() {
    this.dataTableFilterHandler = new DataTableFilterHandler();
    this.dbFilters = await this.dataTableFilterHandler.findFilters('employees') || [];
    let url = '';
    this.dbFilters.forEach((filter) => url = this.calculateUrl(EMPLOYEE_BASE_URL, filter.name, filter.value));
    return url || EMPLOYEE_BASE_URL;
  }
}