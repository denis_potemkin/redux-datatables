export class Filter {
  constructor(tableName, name, value) {
    this.tableName = tableName;
    this.name = name;
    this.value = value;
  }

  equals(other) {
    return this.tableName === other.tableName && this.name === other.name;
  }
}