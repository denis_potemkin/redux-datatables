import './polyfills';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.css';
import { EMPLOYEE_BASE_URL } from './constants';
import { DataTableHandler } from './dataTableHandler';
import { DataTableFilterHandler } from "./dataTableFilterHandler";

let dataTableHandler;

const TABLE_ELEMENT = $('#employees');

$(function () {

  dataTableHandler = new DataTableHandler('employees');

  let dataTableFilterHandler = new DataTableFilterHandler();

  $('#save').on('click', () => {
    dataTableFilterHandler.saveFilters(...dataTableHandler.filters)
  });
  $('#clear').on('click', () => {
    dataTableFilterHandler.clearFilters('employees');
    dataTableHandler.table.columns().every(function () {
      $('input', this.header()).val('');
    });
    dataTableHandler.table.ajax.url(EMPLOYEE_BASE_URL).draw(false);
  });
});
